package com.example.mvcdemo.entity

class DataResult<T> {
    val data: T? = null
    val errorCode: Int? = -99
    val errorMsg: String? = null
}