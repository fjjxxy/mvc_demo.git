package com.example.mvcdemo.login

import com.example.mvcdemo.entity.UserBean

class LoginModel {
    private val api by lazy {
        LoginApi()
    }

    /**
     * 登录
     */
    fun doLogin(
        callback: LoginListener,
        userBean: UserBean
    ) {
        callback.onLoading()
        //开始调用登录的api
        api.doLogin(callback, userBean)
        //后面会有结果，为耗时操作
    }


}