package com.example.mvcdemo.login

interface LoginListener {
    fun onLoginSuccess()

    fun onloginFail()

    fun onLoading()
}