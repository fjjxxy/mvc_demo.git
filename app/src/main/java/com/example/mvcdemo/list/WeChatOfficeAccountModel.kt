package com.example.mvcdemo.list

class WeChatOfficeAccountModel {
    private val weChatOfficeAccountApi by lazy {
        WeChatOfficeAccountApi()
    }

    fun loadData(callback: WeChatOfficeAccountListener) {
        weChatOfficeAccountApi.loadData(callback)
    }
}