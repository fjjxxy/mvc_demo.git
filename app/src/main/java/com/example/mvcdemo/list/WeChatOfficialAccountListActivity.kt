package com.example.mvcdemo.list

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvcdemo.R
import com.example.mvcdemo.entity.OfficeAccountBean
import kotlinx.android.synthetic.main.activity_wechat_official_account.*

class WeChatOfficialAccountListActivity : AppCompatActivity(), WeChatOfficeAccountListener {
    private val mList: MutableList<OfficeAccountBean> = ArrayList()
    private val weChatOfficeAccountModel by lazy {
        WeChatOfficeAccountModel()
    }
    private val weChatOfficialAccountAdapter by lazy {
        WeChatOfficialAccountAdapter(this, mList)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wechat_official_account)
        rv_list.layoutManager = LinearLayoutManager(this)
        rv_list.adapter = weChatOfficialAccountAdapter
        weChatOfficeAccountModel.loadData(this)
    }

    override fun onLoading() {
        Toast.makeText(this, "正在加载中", Toast.LENGTH_SHORT).show()
    }

    override fun onLoadSuccess(list: MutableList<OfficeAccountBean>?) {
        runOnUiThread(Thread {
            Toast.makeText(this, "加载成功", Toast.LENGTH_SHORT).show()
            if (list != null && list.size > 0) {
                mList.addAll(list)
            }
            weChatOfficialAccountAdapter.notifyDataSetChanged()
        })


    }

    override fun onLoadFail(msg: String?) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}