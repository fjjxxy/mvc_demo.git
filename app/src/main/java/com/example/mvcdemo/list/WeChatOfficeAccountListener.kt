package com.example.mvcdemo.list

import com.example.mvcdemo.entity.OfficeAccountBean

interface WeChatOfficeAccountListener {
    fun onLoading()
    fun onLoadSuccess(list: MutableList<OfficeAccountBean>? = null)
    fun onLoadFail(msg: String? = "")
}